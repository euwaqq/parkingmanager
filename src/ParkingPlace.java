public class ParkingPlace {
    public void clear() {
        car=null;
    }

    Car car;
    boolean isForTruck;
    public void info(){
        System.out.println(isForTruck?"Место для грузовиков.":"Место для легковых машин.");
        if (car==null)
            System.out.println("Свободно.");
        else {
            System.out.println("Занято автомобилем №" + car.num);
            System.out.println("Освободится через " + car.parkTime + " ходов");
        }
    }
}
