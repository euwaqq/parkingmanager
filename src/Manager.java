import java.util.Random;
import java.util.Scanner;

public class Manager {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Веедите число парковочных мест для легковых машин.");
        int placesForPassengerCars = sc.nextInt();
        System.out.println("Веедите число парковочных мест для грузовых машин.");
        int placesForTrucks = sc.nextInt();
        ParkingPlace[] parking = new ParkingPlace[placesForPassengerCars + placesForTrucks];
        for (int p = 0; p < placesForPassengerCars + placesForTrucks; p++)
            parking[p] = new ParkingPlace();
        for (int p = 0; p < placesForTrucks; p++)
            parking[p].isForTruck = true;

        Car[] car = new Car[10000];
        int lead = 0;
        int minTruckTime = 12;
        int minPassengerCarTime=12;

        while (true) {
            if (lead > 0)
                for (int p = 0; p < placesForPassengerCars; p++)
                    if ((parking[p].car!=null) && (parking[p].car.parkTime > 0))
                        parking[p].car.parkTime--;

            minTruckTime--;
            minPassengerCarTime--;

            lead++;
            System.out.println(lead + "-й ход.");

            minTruckTime=carCome(minTruckTime,parking,car,0,placesForTrucks);
            minPassengerCarTime=carCome(minPassengerCarTime,parking,car,placesForTrucks, parking.length);

            goAway(parking);

            int x = 0;
            {while (x != 1) {
                System.out.println();
                System.out.println("Чтобы прейти к следующему ходу, введите 1");
                System.out.println("Чтобы получить информацию о парковке, введите 2");
                System.out.println("Чтобы очисть парковку, введите 4");
                x = sc.nextInt();
                if (x == 2)
                    takeness(placesForTrucks, placesForPassengerCars, parking,minTruckTime,minPassengerCarTime);
                if (x == 4)
                    for (ParkingPlace parkingPlace : parking) parkingPlace.clear();
                }

            }
        }
    }

    public static int carCome(int min,ParkingPlace[] parking, Car[] car,int p, int end) {
        Random r = new Random();
        int Cars = r.nextInt(parking.length / 3);//Создаются легковые авто
        int n;
        for (int c = 0; c < Cars; c++) {
            n = r.nextInt(9999);
            while ((n < 1000) | (car[n] != null))
                n = r.nextInt(9999);
            car[n] = new Car(n);
            if (end< parking.length)
                car[n].isTruck=true;

            //Заезжают на парковку
            while ((!car[n].isParked) & (p < end)) {
                if (parking[p].car==null) {
                    car[n].parkPlace = p;
                    parking[p].car = car[n];
                    car[n].isParked = true;
                    if(end< parking.length)
                        System.out.print("Грузовик");
                    else
                        System.out.print("Легковушка");
                    System.out.println(" с номером " + n + " заезжает на " + p + "-е место на " + car[n].parkTime + " ходов.");
                }
                p++;
            }
            if (!car[n].isParked)//Грузовики заезжают на места для пассажирских авто
            while ((!car[n].isParked) & (p < parking.length - 1))
                if ((parking[p].car==null) & (parking[p + 1].car==null)) {
                    car[n].parkPlace = p;
                    parking[p].car = car[n];
                    parking[p + 1].car = car[n];
                    car[n].isParked = true;
                    System.out.println("Грузовик с номером " + n + " заехал на " + p + "-е и " + p + 1 + "-е места на " + car[n].parkTime + " ходов.");
                }
            if (!car[n].isParked) {  //Не хватило мест
                System.out.println("Car number " + n + " took French leave.");
            }else
                if (car[n].parkTime < min)
                    min = car[n].parkTime;
        }
        return min;
    }

    public static void goAway(ParkingPlace[] parking){
        //Уезжают с порковки
        for (int p = 0; p < parking.length; p++)
            if ((parking[p].car!=null) && (parking[p].car.parkTime == 0)) {
                System.out.println("Автомобиль с номерным знаком " + parking[p].car.num + " уехал с " + p + "-го места.");
                parking[p].clear();
            }
    }

    public static void takeness(int tPlaces, int pPlaces, ParkingPlace[] parking,int tMin,int pMin) {
        int freeTrackPlaces = 0;
        int takenTrackPlaces = 0;
        int freePassengerCarPlaces = 0;
        int takenPassengerCarPlaces = 0;
        for (int i = 0; i < tPlaces; i++)
            if (parking[i].car==null)
                freeTrackPlaces++;
            else
                takenTrackPlaces++;
        System.out.println(takenTrackPlaces + " грузовых мест занято, " + freeTrackPlaces + " свободно");
        if(takenTrackPlaces>0)
            System.out.println("Ближайшее место для грузовых освободится через " + tMin + " ходов.");

        for (int i = tPlaces; i < tPlaces + pPlaces; i++)
            if (parking[i].car==null)
                freePassengerCarPlaces++;
            else
                takenPassengerCarPlaces++;
        System.out.println(takenPassengerCarPlaces + " легковых мест занято, " + freePassengerCarPlaces + " свободно");
        if(takenPassengerCarPlaces>0)
            System.out.println("Ближайшее место для легковых автомобилей освободится через " + pMin + " ходов.");

        for (ParkingPlace parkingPlace : parking) parkingPlace.info();

    }
}